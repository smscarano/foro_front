let api_url ='http://foroapi.ydns.eu/api/';
//let api_url = 'http://127.0.0.1:8000/api/';

createSpinner = function(){
    let spinner_container = document.createElement("div");
    spinner_container.classList.add("d-flex");
    spinner_container.classList.add("justify-content-center");
    spinner_container.id = "spinner_container";

    let spinner_div = document.createElement("div");
    spinner_div.id = "spinner_div";
    spinner_div.classList.add("text-primary");
    spinner_div.innerHTML = '<i class="fas fa-spinner fa-spin"></i>';
    spinner_container.appendChild(spinner_div);
    return spinner_container;
}

destroySpinner = function(){
    let spinner = document.getElementById("spinner_container");
    if(spinner)spinner.remove();
}

checkLogin = function(){
    if( localStorage.getItem('api_token') ) return true;
    return false;
};

loginHandler = function(){
    document.getElementById("login_button").disabled = true;

    let username = document.getElementById("username");
    let username_value = String( username.value  ).trim();
    let user_min_length = username.getAttribute('minlength');
    let login_div = document.getElementById("login");

    if( username_value.length < user_min_length ) {
        document.getElementById("login_button").disabled = false;

        if(!document.getElementById("username_error")){
            let username_error_div = document.createElement("div");
            username_error_div.innerText = "El usuario debe contener al menos " + user_min_length + " caracteres";
            username_error_div.id = "username_error";
            username_error_div.classList.add("error");

            login_div.prepend(username_error_div);
            setTimeout(() => {
                username_error_div.remove();
            }, 10000);
        }
        return false;
    }
    
    let password = document.getElementById("password");
    let password_value = String( password.value  ).trim();
    let passwd_min_length = password.getAttribute('minlength')
    
    if( password_value.length < passwd_min_length ) {
        document.getElementById("login_button").disabled = false;
        if(!document.getElementById("password_error")){
            let password_error_div = document.createElement("div");
            password_error_div.innerText = "La clave debe contener al menos " + passwd_min_length + " caracteres";
            password_error_div.id = "password_error";
            password_error_div.classList.add("error");

            login_div.prepend(password_error_div);
            setTimeout(() => {
                password_error_div.remove();
            }, 10000);
        }
        return false;
    }

    var xhr = new XMLHttpRequest();
    xhr.open("POST", api_url + 'token', true);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let successful_response = JSON.parse(xhr.response);
            if(successful_response.success === 1){
                localStorage.setItem('id',successful_response.user.id);
                localStorage.setItem('name',successful_response.user.name);
                localStorage.setItem('email',successful_response.user.email);
                localStorage.setItem('api_token',successful_response.user.api_token);
                localStorage.setItem('role',successful_response.user.role);

                login_div.remove();
                createNavBar();
                createPosts();
            }
            else{
                document.getElementById("login_button").disabled = false;
                
                let login_error_div = document.createElement("div");
                login_error_div.innerText = "usuario o contraseña incorrectos";
                login_error_div.id = "password_error";
                login_error_div.classList.add("error");

                login_div.prepend(login_error_div);
                setTimeout(() => {
                    login_error_div.remove();
                }, 10000);
            }
        }
    }
   
    let post_data = {email:username_value,password:password_value};
    xhr.send( JSON.stringify(post_data) );

}

createLogin = function(){
    let login_div = document.createElement("div");             
    login_div.id = "login";
    login_div.classList.add("text-center");
    login_div.classList.add("container");
    
    let username_div = document.createElement("div");             
    username_div.classList.add("row-no-gutters");

    let username = document.createElement("INPUT");
    username.setAttribute("type", "text");
    username.setAttribute("placeholder", "Usuario");
    username.setAttribute("minlength", "6"); 
    username.classList.add("form-control");
             
    username.id = "username";
    username_div.appendChild(username);

    let password_div = document.createElement("div");             
    password_div.classList.add("row-no-gutters");

    let password = document.createElement("INPUT");
    password.setAttribute("type", "password");
    password.setAttribute("placeholder", "Clave");
    password.setAttribute("minlength", "6");             
    password.classList.add("form-control");

    password.id = "password";
    password_div.appendChild(password);

    let button_div = document.createElement("div");             
    button_div.classList.add("row-no-gutters");
    
    let submit_button = document.createElement("button");
    submit_button.id = "login_button";
    submit_button.innerText = "Enviar";
    submit_button.addEventListener("click",loginHandler);
    submit_button.classList.add("btn-primary");

    button_div.appendChild(submit_button);

    login_div.appendChild(username_div);

    login_div.appendChild(password_div);

    login_div.appendChild(button_div);

    document.body.appendChild(login_div); 
}

destroyLogin = function(){
    let login_div = document.getElementById("login");
    login_div.remove();
}

createPosts = function(options){
    let posts = document.getElementById('posts');
    if(posts) posts.remove();
    let inicio_li_a = document.getElementById("inicio_li_a");
    inicio_li_a.removeEventListener("click",irAlInicio);
    var xhr = new XMLHttpRequest();
    if( options && 'page' in options ) {
        xhr.open("GET", api_url + 'posts?page=' + options.page, true);
    }
    else {
        xhr.open("GET", api_url + 'posts', true);
    }
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("api_token"));
    xhr.send();
    let spinner = createSpinner();
    document.body.appendChild(spinner);

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let successful_response = JSON.parse(xhr.response);

            let posts_div = document.createElement("div");             
            posts_div.id = "posts";
            posts_div.classList.add("text-justify");
            posts_div.classList.add("container");
            
            for (let index = 0; index < successful_response.data.length; index++) {
                const element = successful_response.data[index];
                let element_div = document.createElement("div");             
                element_div.classList.add("row-no-gutters");
                element_div.classList.add("post");
                element_div.id = "post_id" + index;
                element_div.addEventListener("click",function(){
                    verPostHandler(element.id,element_div.id);
                });

                element_div.innerHTML = '<h5>' + element.title + '</h5>';
                element_div.id = 'post' + index;
                
                let date_string = formatDate(element.created_at);

                let small = document.createElement("small");
                small.innerHTML = 'Escrito por ' + element.name + ' ' + date_string + '<br>' + element.responses_count + ' respuestas <br>';
                element_div.prepend(small);

                /*let responses = document.createElement("small");
                responses.innerHTML = element.responses_count + ' respuestas <br>';
                element_div.append(responses);*/

                posts_div.appendChild(element_div);
            }
            let footer_div = document.createElement("div");             
            footer_div.classList.add("row-no-gutters");
            footer_div.classList.add("post");
            footer_div.id = "footer_posts";
            let footer_info = document.createElement("span");
            footer_info.innerHTML = 'Viendo página ' + successful_response.current_page + ' de ' + successful_response.last_page + '&nbsp ';
            
            let next_page_tmp = successful_response.next_page_url;
            if(next_page_tmp){
                let page_number_tmp = next_page_tmp.split("=");
                let next_page = page_number_tmp[1];
                let prox_pag_a = document.createElement("a");
                prox_pag_a.addEventListener("click",function(){
                    createPosts({page:next_page});
                });
                prox_pag_a.id = "prox_pag_a";
                prox_pag_a.href = "#";
                prox_pag_a.innerHTML = '<i class="fas fa-arrow-circle-right"></i>';
                footer_info.append(prox_pag_a);
            }
            
            let prev_page_tmp = successful_response.prev_page_url;
            if(prev_page_tmp){
                let page_number_tmp = prev_page_tmp.split("=");
                let prev_page = page_number_tmp[1];
                let prev_page_a = document.createElement("a");
                prev_page_a.addEventListener("click",function(){
                    createPosts({page:prev_page});
                });
                prev_page_a.id = "prev_pag_a";
                prev_page_a.href = "#";
                prev_page_a.innerHTML = '<i class="fas fa-arrow-circle-left"></i>&nbsp';
                footer_info.prepend(prev_page_a);
            }

            footer_div.appendChild(footer_info);
            posts_div.appendChild(footer_div);

            destroySpinner();
            document.body.appendChild(posts_div);
            if(document.getElementById("prox_pag")) document.getElementById("prox_pag").addEventListener("click",function(){
                alert(next_page);
            });

            inicio_li_a.addEventListener("click",irAlInicio);
        }
        if (this.readyState === XMLHttpRequest.DONE && this.status === 401) {
            destroySpinner();
            cerrarSesion();
        }
    }
}

formatDate = function(date_string){
    var d = new Date(date_string);
    let day = String( creation_date_string = d.getDate() );
    if( day.length == 1 ) day = "0" + day;
    let month = String( creation_date_string = d.getMonth() );
    if( month.length == 1 ) month = "0" + month;
    let year = String( creation_date_string = d.getFullYear() );
    let hours = String( creation_date_string = d.getHours() );
    if( hours.length == 1 ) hours = "0" + hours;
    let minutes = String( creation_date_string = d.getMinutes() );
    if( minutes.length == 1 ) minutes = "0" + minutes;
    let date_parsed = day + '/' + month + '/' + year + ' ' + hours + ':' + minutes;
    return date_parsed;
}

createNavBar = function(){
    let nav_div = document.createElement("nav");
    nav_div.id = "navbar";
    nav_div.classList.add("navbar");
    nav_div.classList.add("navbar-expand-sm");
    nav_div.classList.add("bg-primary");

    let ul_elem = document.createElement("ul");
    ul_elem.classList.add("navbar-nav");
    nav_div.appendChild(ul_elem);

    let inicio_li = document.createElement("li");
    inicio_li.classList.add("navbar-item");
    ul_elem.appendChild(inicio_li);

    let inicio_li_a = document.createElement("a");
    inicio_li_a.classList.add("navbar-link");
    inicio_li_a.addEventListener("click",irAlInicio);
    inicio_li_a.id = "inicio_li_a";
    inicio_li_a.href = "#";
    inicio_li_a.innerHTML = '<i class="fas fa-home"></i> Inicio';
    inicio_li.appendChild(inicio_li_a);

    let nuevo_post_li = document.createElement("li");
    nuevo_post_li.classList.add("navbar-item");
    ul_elem.appendChild(nuevo_post_li);

    let nuevo_post_a = document.createElement("a");
    nuevo_post_a.classList.add("navbar-link");
    nuevo_post_a.addEventListener("click",nuevoPost);
    nuevo_post_a.href = "#";
    nuevo_post_a.innerHTML = ' <i class="fas fa-envelope"></i> Preguntar';
    nuevo_post_li.appendChild(nuevo_post_a);

    let cerrar_sesion_li = document.createElement("li");
    cerrar_sesion_li.classList.add("navbar-item");
    ul_elem.appendChild(cerrar_sesion_li);

    let cerrar_sesion_a = document.createElement("a");
    cerrar_sesion_a.classList.add("navbar-link");
    cerrar_sesion_a.href = "#";
    cerrar_sesion_a.addEventListener("click",cerrarSesion);
    cerrar_sesion_a.innerHTML = '<i class="fas fa-times"></i> Cerrar sesión';
    cerrar_sesion_li.appendChild(cerrar_sesion_a);

    document.body.appendChild(nav_div);
}

nuevoPost = function(){
    if(document.getElementById("nuevo_post")) return false;
    let post_container = document.getElementById('post_container');
    if(post_container) post_container.remove();
    let respuestas  = document.getElementById("responses");
    if(respuestas) respuestas.remove();

    let posts = document.getElementById('posts');
    if(posts) posts.remove();
    
    let nuevo_post_container = document.createElement("div");
    nuevo_post_container.classList.add("text-justify");
    nuevo_post_container.classList.add("container");
    nuevo_post_container.id = "nuevo_post";
    document.body.appendChild(nuevo_post_container);

    let form_div = document.createElement("div");             
    form_div.classList.add("row-no-gutters");
    nuevo_post_container.appendChild(form_div);

    let title = document.createElement("INPUT");
    title.id="post_title";
    title.classList.add("form-control");
    title.setAttribute("type", "text");
    title.setAttribute("placeholder", "Título");
    title.setAttribute("minlength", "4");                       
    form_div.appendChild(title);

    let body = document.createElement("TEXTAREA");
    body.id="post_body";
    body.classList.add("form-control");
    body.setAttribute("placeholder", "Cuerpo");
    body.rows = 10;
    form_div.appendChild(body);

    let submit_button = document.createElement("button");
    submit_button.innerText = "Enviar";
    submit_button.addEventListener("click",crearPostHandler);
    submit_button.classList.add("btn-primary");
    submit_button.id = "add_post_button";
    form_div.appendChild(submit_button);
}

crearPostHandler = function(){
    let submit_button = document.getElementById("add_post_button");
    let post_container = document.getElementById('post_container');
    if(post_container) post_container.remove();
    let title = String (document.getElementById("post_title").value).trim();
    let body = String (document.getElementById("post_body").value).trim().replace(/\r?\n/g, '<br />');
    let nuevo_post_div = document.getElementById("nuevo_post")
    if(title.length < 3){
        let title_error_div = document.createElement("div");
        title_error_div.innerText = "El título debe contener al menos " + 3 + " caracteres";
        title_error_div.id = "title_error_div";
        title_error_div.classList.add("error");
        nuevo_post_div.prepend(title_error_div);
        setTimeout(() => {
                title_error_div.remove();
            }, 10000);
        return false;
    }
    if(body.length < 3){
        let body_error_div = document.createElement("div");
        body_error_div.innerText = "El mensaje debe contener al menos " + 3 + " caracteres";
        body_error_div.id = "title_error_div";
        body_error_div.classList.add("error");
        let nuevo_post_div = document.getElementById("nuevo_post")
        nuevo_post_div.prepend(body_error_div);
        setTimeout(() => {
            body_error_div.remove();
            }, 10000);
        return false;
    }
    submit_button.disabled = true;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", api_url + 'posts', true);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("api_token"));

    let post_data = {title:title,body:body,author:localStorage.getItem("id")};
    xhr.send( JSON.stringify(post_data) );

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let nuevo_post = document.getElementById('nuevo_post');
            if(nuevo_post) nuevo_post.remove();
            createPosts();
        }
        else{
            submit_button.disabled = false;
        }
    }
}

verPostHandler = function(id,div_id){
    document.getElementById(div_id).removeEventListener("click",function(){
        verPostHandler(id);
    });
    var xhr = new XMLHttpRequest();
    xhr.open("GET", api_url +'posts/' + id, true);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("api_token"));
    xhr.send();
    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let post = JSON.parse(xhr.response);
            let posts = document.getElementById('posts');
            if(posts) posts.remove();
            if(document.getElementById("post_container")){
                return false;
            }
            let post_container = document.createElement("div");             
            post_container.id = "post_container";
            post_container.classList.add("container");

            document.body.append(post_container);

            let content_div = document.createElement("div");             
            content_div.classList.add("row-no-gutters");
            post_container.appendChild(content_div);

            let date_string = formatDate(post[0].created_at);

            var small = document.createElement("small");
            small.innerHTML = 'Escrito por ' + post[0].name + ' ' + date_string + '<br>';
            content_div.appendChild(small);

            let title = document.createElement("span");
            title.innerHTML = "<h4>" + post[0].title + "</h4>";
            content_div.appendChild(title);

            let body = document.createElement("p");
            body.innerHTML = post[0].body ;
            content_div.appendChild(body);    

            let responder_button = document.createElement("button");
            responder_button.innerText = "responder";
            responder_button.addEventListener("click",function(){responderPostHandler(post[0].id)});
            responder_button.classList.add("btn-primary");
            content_div.appendChild(responder_button);

            let borrar_button = document.createElement("button");
            borrar_button.innerText = "Borrar";
            borrar_button.id="borrar_post_button";
            borrar_button.addEventListener("click",function(){borrarPostHandler(post[0].id)});
            borrar_button.classList.add("btn-danger");
            if( post[0].author == localStorage.getItem("id") || localStorage.getItem("role") == "admin" ) content_div.appendChild(borrar_button);
            crearRespuestas(post[0].id);
        }
        if (this.readyState === XMLHttpRequest.DONE && this.status === 401) cerrarSesion();
    }
}

crearRespuestas = function(id){
    let responses_div  = document.getElementById("responses");
    if(responses_div) responses_div.remove();
    
    let xhr = new XMLHttpRequest();
    xhr.open("GET", api_url + 'responses?id='+id, true);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("api_token"));
    xhr.send();
    let spinner = createSpinner();
    document.body.appendChild(spinner);

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let successful_response = JSON.parse(xhr.response);
            let responses_div = document.createElement("div");             
            responses_div.id = "responses";
            responses_div.classList.add("text-justify");
            responses_div.classList.add("container");

            for (let index = 0; index < successful_response.length; index++) {
                const element = successful_response[index];

                let element_div = document.createElement("div");
                element_div.id = 'response' + index;
                
                let date_string = formatDate(element.created_at);

                var small = document.createElement("small");
                small.innerHTML = 'Escrito por ' + element.name + ' ' + date_string + '<br>';

                element_div.classList.add("row-no-gutters");
                element_div.classList.add("response");
                element_div.innerHTML = '<p>' + element.body + '</p>';

                element_div.prepend(small);
                responses_div.appendChild(element_div);
                
                if( localStorage.getItem("role") == "admin" ){
                    let borrar_respuesta_button = document.createElement("button");
                    borrar_respuesta_button.id = "borrar_respuesta_button";
                    borrar_respuesta_button.innerText = "Borrar";
                    borrar_respuesta_button.addEventListener("click",function(){
                        borrarRespuesta(element.id)
                        element_div.remove();
                    });
                    borrar_respuesta_button.classList.add("btn-danger");
                    borrar_respuesta_button.classList.add("small");
                    element_div.prepend(borrar_respuesta_button);
                }
            }
            destroySpinner();
            document.body.appendChild(responses_div);
        }
    }
}

responderPostHandler = function(id){
    if(document.getElementById("respuesta_div")){
        document.getElementById("respuesta_div").scrollIntoView({ behavior: 'smooth', block: 'end'});
        return false;
    } 
    
    let respuesta_container = document.createElement("div");
    respuesta_container.classList.add("text-justify");
    respuesta_container.classList.add("container");
    respuesta_container.id = "respuesta_div";
    document.body.appendChild(respuesta_container);

    let respuesta_div = document.createElement("div");             
    respuesta_div.classList.add("row-no-gutters");
    respuesta_container.appendChild(respuesta_div);

    let body = document.createElement("TEXTAREA");
    body.id="response_body";
    body.classList.add("form-control");
    body.setAttribute("placeholder", "Responder");
    body.rows = 5;
    body_with_breaks = String( body.value ).replace(/\r?\n/g, '<br />');

    respuesta_div.appendChild(body);
    let enviar_respuesta_button = document.createElement("button");
    enviar_respuesta_button.innerText = "enviar";
    enviar_respuesta_button.id="enviar_respuesta_button";
    enviar_respuesta_button.addEventListener("click",function(){enviarRespuestaHandler(id,body.value.replace(/\r?\n/g, '<br />'))});
    enviar_respuesta_button.classList.add("btn-primary");
    respuesta_div.appendChild(enviar_respuesta_button);

    respuesta_div.scrollIntoView({ behavior: 'smooth', block: 'end'});
}

enviarRespuestaHandler = function(id,body){
    let enviar_respuesta_button = document.getElementById("enviar_respuesta_button");
    enviar_respuesta_button.disabled = true;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", api_url + 'responses', true);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("api_token"));

    let post_data = {parent:id,body:body,author:localStorage.getItem("id")};
    xhr.send( JSON.stringify(post_data) );

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                let textarea_respuesta = document.getElementById("respuesta_div");
                if(textarea_respuesta) textarea_respuesta.remove();
                crearRespuestas(id);
        }
        enviar_respuesta_button.disabled = false;
    }
}

borrarPostHandler = function(id){
    
    let borrar_post_button = document.getElementById("borrar_post_button");
    borrar_post_button.disabled = true;

    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", api_url + 'posts/' + id, true);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("api_token"));
    xhr.send();
    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let post_container = document.getElementById('post_container');
            if(post_container) post_container.remove();
            let responses_div  = document.getElementById("responses");
            if(responses_div) responses_div.remove();
            createPosts();
        }
        borrar_post_button.disabled = false;
    }
}

borrarRespuesta = function(id){
    
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", api_url + 'responses/' + id, true);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("api_token"));
    xhr.send();
    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
        }
    }
}

irAlInicio = function(){
    let posts = document.getElementById('posts');
    if(posts) posts.remove();
    let nuevo_post = document.getElementById('nuevo_post');
    if(nuevo_post) nuevo_post.remove();
    let post_container = document.getElementById('post_container');
    if(post_container) post_container.remove();
    let respuestas  = document.getElementById("responses");
    if(respuestas) respuestas.remove();
    let respuesta_div  = document.getElementById("respuesta_div");
    if(respuesta_div) respuesta_div.remove();
    
    createPosts();

}
cerrarSesion = function(){
    localStorage.clear();
    let posts = document.getElementById('posts');
    if(posts) posts.remove();
    let navbar = document.getElementById('navbar');
    if(navbar) navbar.remove();
    let nuevo_post = document.getElementById('nuevo_post');
    if(nuevo_post) nuevo_post.remove();
    let post_container = document.getElementById('post_container');
    if(post_container) post_container.remove();
    
    let respuesta_div = document.getElementById('respuesta_div');
    if(respuesta_div) post_container.remove();
    let respuestas  = document.getElementById("responses");
    if(respuestas) respuestas.remove();
    
    createLogin();

}

onLoadHandler = function(){
    if ( !checkLogin() ) createLogin();
    else{
        createNavBar();
        createPosts();
    }
}
